
def convert_to_roman(number, rom_denom, rom_val):
    if number == 0:
        return ''
    else:
        rom_str = (rom_denom[0] * (number//rom_val[0])) + convert_to_roman(number % 
        rom_val[0], rom_denom[1:], rom_val[1:]) 
    return rom_str

rom_denom = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I']
rom_val = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
number = int(input("Enter numeral to convert: "))
rom_str = convert_to_roman(number, rom_denom, rom_val)
print(rom_str)